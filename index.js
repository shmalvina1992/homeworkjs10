"use strict"

const mainList = document.querySelector('.tabs');
const mainListItems = document.querySelectorAll('.tabs-title');
const secondaryList = document.querySelector('.tabs-content');
const secondaryListItems = document.querySelectorAll('.hidden');

function switchTabs() {
    mainList.addEventListener('click', e => {
        mainListItems.forEach(item => {
            if (e.target === item) {
                item.classList.add('active');
            } else {
                item.classList.remove('active');
            }

            // if (item.classList.contains('active')) {
            //     item.classList.remove('active');
            // } 
        });
        
        // if (!e.target.classList.contains('active')) {
        //     e.target.classList.add('active')
        // }
    
        secondaryListItems.forEach(item => {
            if (e.target.dataset.tab === item.dataset.tab) {
                item.classList.add('active');
            } else {
                item.classList.remove('active');
            }
        })
    });
}

switchTabs();

// mainList.addEventListener('click', e => {
//     mainListItems.forEach(item => {
//         if (e.target === item) {
//             item.classList.add('active');
//         } else {
//             item.classList.remove('active');
//         }
//     });
    
//     secondaryListItems.forEach(item => {
//         if (e.target.dataset.tab === item.dataset.tab) {
//             item.classList.add('active');
//         } else {
//             item.classList.remove('active');
//         }
//     })
// });

